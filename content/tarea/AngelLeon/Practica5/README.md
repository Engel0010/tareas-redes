# Práctica 5
*León Canto Angel
*311253171

***



Para ejecutar esta practica tenemos que posicionarnos en la carpeta que se encuentra el codigo fuente desde la terminal ejemplo: tareas-redes/content/tarea/AngelLeon

ejecutar el archivo para compilar con gcc lso diferentes archivos fuentes
para ejecutar  es con el comando ./<nombre del archivo obejeto>

Si se desea compilar con el  archivo makefile es come makefile y se compila


##DNS

Se Registra el subdominio
![](Imagenes/P5-R-Dominio)

Se agrega el dominio
![](Imagenes/RedesDominio.png)

![](Imagenes/P5-R3.png)


###Preguntas
¿Qué es un servidor DNS?

“Sistema de Nombre de Dominio”, este término hace referencia al sistema en general en el que está basado el funcionamiento de los dominios en Internet: una red mundial de servidores que traducen nombres que tú como humano entiendes, a direcciones IP que las máquinas entienden.

¿Para qué se utiliza cada uno de los registros SOA, NS, A, AAAA, PTR, CNAME, MX y TXT?

SOA
Un registro "SOA" significa "Comienzo de autoridad". Evidentemente, es uno de los registros DNS más importantes, dado que guarda información esencial, como la fecha de la última actualización del dominio y otros cambios y actividades
NS
El registro "NS" significa Servidor de nombres e indica qué nombre del servidor es el autorizado para el dominio.
AAAA
El registro "A" hace referencia a la Dirección y es el tipo más básico de sintaxis de DNS.  Indica la dirección de IP real de un dominio
AAAA
El registro "AAAA" (también conocida como dirección IPV6) indica el nombre de alojamiento a una dirección IPv6 de 128 bits.  Las direcciones DNS normales se mapean para direcciones IPv4 de 32 bits.
PTR
El registro "PTR" significa "Punto de terminación de red". La sintaxis de DNS es responsable del mapeo de una dirección Ipv4 para el CNAME en el alojamiento.
CNAME
El registro de "CNAME" significa nombre canónico y su función es hacer que un dominio sea un alias para otro. El CNAME generalmente se utiliza para asociar nuevos subdominios con dominios ya existentes de registro A.
MX
El registro "MX" o intercambio de correo es principalmente una lista de servidor de intercambio de correo que se debe utilizar para el dominio.
TXT
Un registro "TXT" significa "Texto". Esta sintaxis de DNS permite que los administradores inserten texto en el registro DNS. A menudo se utiliza para denotar hechos o información sobre el dominio

¿Qué hacen las herramientas nslookup y dig?

El comando dig (Domain informatioI Groper) constituye una herramienta para realizar consultas de diverso tipo a un servidor de DNS. Este muestra las respuestas recibidas de acuerdo a su solicitud. Es muy útil para detectar problemas en la configuración de los servidores de DNS debido a su flexibilidad, facilidad de uso y claridad en su salida

EL comando nslookup posibilita la consulta de  un servicio de nombres y adquirir informacion relacionado con el dominio o el host y asu diagnosticar los ocasionales problemas de configuración que pueden surgir del DNS. Sirve para saber si el DNS resuelve correctamente  los nombres y las dirrecciones IP,





###  Packet Tracer

####Creacion de Redes


Asignar direcciones IP a PC1 y PC2 de manera estática, junto con un gateway y un servidor DNS.

![](Imagenes/P5-R-4.png)

Se verifica que se comuniquen la PC

![](Imagenes/P5-R-Tra2.png)





Conectar una PC, una impresora y un servidor a un swicht
Se verifica que este bien conectado

![](Imagenes/P5-R-Trace6.png)


Se le asigna una dirección IP estática al servidor

![](Imagenes/P5-R-tra4.png)

Indicar que se le asigne una dirección IP de manera dinámica a PC2 (a través de DHCP)

![](Imagenes/P5-R-Tra5.png)


Verificar que PC2 puede comunicarse con Printer0

![](Imagenes/P5-R-Tra6.png)

Queda asi las dos conexiones 

![](Imagenes/packettracer.png)


Asignar dirección IP a servidor web www.redes.net

![](Imagenes/www.redes.net)

Asignar dirección IP a servidor DNS

Verificar comunicación entre ambos servidores

Queda asi

![](Imagenes/packet.png)

Asignan direcciones IP a interfaces de routers y prenderlas, esto puede hacerse por medio de interfaz gráfica o línea de comandos, se mostrará la configuración de Router0 por línea de comandos:


![](Imagenes/P5-R-Router.png)

Verificar que desde el navegador web de PC1 y PC2 se pueda acceder a www.redes.net:

![](Imagenes/P5-PTcisco6.png)

![](Imagenes/P5-PTCisco.png)



###Preguntas


#####¿Qué diferencia hay entre un switch y un router?

La principal diferencia es que a través del Switch la información enviada por el ordenador de origen va directamente al ordenador de destino sin replicarse en el resto de equipos que estén conectados.

Un Router se utiliza para elegir la ruta más pequeña para que un paquete llegue a su destino. Un Switch almacena el paquete recibido, lo procesa para determinar su dirección de destino y lo reenvía a un destino específico.

La diferencia básica entre un router y un switch es que un router conecta diferentes redes entre sí, mientras que un switch conecta múltiples dispositivos entre sí para crear una red.


#####¿Qué tipos de switches hay?

#####Acorde a la forma en que se envían los datos

#####Cut through

Estos se fabricaron como mejora de los switches store and forward. Estos solo procesan los primeros bytes de datos. Estos bytes almacenan los datos tomando en cuenta el lugar donde llegará la información. Este posee un cut though que fue adicionado para evitar que los datos erróneos ocupen un espacio en la red y así se evite traspasarlos a través de la red.

#####Store and forward

En este los datos se guardan en un buffer, una vez allí se mide la información rechazando los datos que no sean correctos, y luego se envía a otra computadora. En estos la información se traslada mucho mas rápido, ya que no hay errores en traspaso de datos erróneos. Son de gran uso en corporaciones grandes.

#####Cut through adaptivo

Este llega  a trabajar tanto con los switch store and forward como con los de cut throught, empleando una técnica en específica según la cantidad de información que se enviará. En presencia de muchos datos erróneos, este trabaja en modo store and forward hasta que se normalice la red. Es el tipo de switch de mayor utilización, ya que puede trabajar sobre una red LAN.

#####Según el tipo de administración
#####Switch administrable

Refiere al tipo de switch que permite que sea administrado.

#####Switch no administrable

Estos no permiten ningún tipo de administración o de configuración.

#####Acorde a su capacidad
#####Swith apilables

Estos agrupan diversas unidades en un bus de expansión, el cual aporta un gran ancho de banda con lo cual se mejora la comunicación full-duplex.

#####Switch no apilable

Estos swith no soportan ningún bus de expansión.


#####Otros tipos de swith
#####Switch troncal

Son aquellos empleados en el núcleo de las redes amplias. Los servidores, los routers y otros swithes se llegan a conectar a este tipo de switch.

#####Switch perimetral

Estos son los que se conectan a la red troncal, por lo cual se usan en el nivel jerárquico inferior. A estos se conectan los equipos finales de los usuarios.

#####Switch gestionable

También conocido como managed en inglés. Se trata de un tipo de switch que presenta características añadidas que necesitan de una gestión y configuración.

#####Swith no gestionable

Refiere aquellos switch que solo ofrecen funcionalidades básicas, las cuales no requieren de una gestión o configuración. Estos también se le suele conocer en el idioma inglés como managed.

#####Switch desktop

Es el más básico a comparación de los demás switch, el cual lleva a cabo la conmutación básica y carece de características adicionales. Se suele emplear en redes domésticas o en redes de empresas pequeñas, permitiendo la conexión de pocos equipos. La configuración de cada puerto se realiza por una auto-configuración de Ethernet, por lo tanto no requiere de configuración alguna.

#####Switches perimetrales no gestionables

Estos se emplean en redes de tamaño pequeño, son parecidos a los switches desktop en cuanto a funcionamiento, ya que no permite opciones de configuración, auto-configurándose el mismo. La diferencia de este es que el número de puertos es mucho mayor, por lo tanto se puede montar en rack 19¨.

#####Switches perimetrales gestionables

Son los empleados para la conexión de equipos en redes que presenten un tamaño medio o grande. Se ubican en el nivel jerárquico inferior. Estos funcionan bajo opciones avanzadas de gestión y configuración.

#####Switches troncales de prestaciones medias

Refiere al tipo de switch que forma un troncal o núcleo en una red de medio tamaño. Este llega a ofrecen funcionalidades y prestaciones muy avanzadas. Estos brindan como enrutamiento IP características de tercer nivel, punto que le hace diferenciar de los switches perimetrales.

#####Switch troncal de alta prestación

Se caracteriza principalmente por su gran rendimiento y su alta modularidad. Este se presenta en varios formatos, pero el más común es el de tipo chasis, que es el lugar donde los módulos son instalados. Este tipo de switch es el que se emplea en redes corporativas grandes o en redes de campos y metropolitanas.

#####Switch procurve 1700-8

Se trata de un switch híbrido que es  gestionable y que presenta opciones muy avanzadas y que tiene 8 puertos, donde uno es de 10/100/1000, y los restante son de 10/100.



#####¿Qué es una lista de acceso y para qué se utilizan?

Una Lista de Control de Acceso o ACL (del inglés, Access Control List) es un concepto de seguridad informática usado para fomentar la separación de privilegios. Es una forma de determinar los permisos de acceso apropiados a un determinado objeto, dependiendo de ciertos aspectos del proceso que hace el pedido.

Las ACLs permiten controlar el flujo del tráfico en equipos de redes, tales como routers y switches. Su principal objetivo es filtrar tráfico, permitiendo o denegando el tráfico de red de acuerdo a alguna condición. Sin embargo, también tienen usos adicionales, como por ejemplo, distinguir “tráfico interesante” (tráfico suficientemente importante como para activar o mantener una conexión) en ISDN.




#####Se creó una lista de acceso con el comando access-list 1 permit 192.168.1.0 0.0.0.255, la última parte del comando (0.0.0.255) es una wildcard, ¿qué es una wildcard  y en qué se diferencia con una máscara de red?

Una máscara wildcard es una máscara de bits que indica qué partes de una dirección de IP son relevantes para la ejecución de una determinada acción

Wildcards define un host o un número de hosts en una subred u octeto y son mucho más flexibles que las máscaras de subred, ya que no hay requisito de que los bits a 1 estén continuos. Otro punto que se diferencia es que los bits a tener en cuenta son al contrario que subnetmask, es decir, que el que tiene peso es el 0 y no el 1. Es muy típico en las listas de acceso (ACLs) que las wildcards aparezcan como la inversa de subnetmak, por ejemplo, 0.0.0.255 coincide con cualquier valor para el último octeto de una dirección IP en una subnetmask 255.255.255.0. Pero esto no es estrictamente necesario. Se puede conseguir todo tipo de extrañas coincidencias con una wildcard

#####¿Qué diferencia hay entre una dirección IP pública y una privada?

IP Pública

Es la que tiene asignada cualquier equipo o dispositivo conectado de forma directa a Internet.

Algunos ejemplos son: los servidores que alojan sitios web como Google, los router o modems que dan a acceso a Internet, otros elementos de hardware que forman parte de su infraestructura, etc.

Las IP públicas son siempre únicas. No se pueden repetir. Dos equipos con IP de ese tipo pueden conectarse directamente entre sí. Por ejemplo, tu router con un servidor web. O dos servidores web entre sí.
IP Privada

Se utiliza para identificar equipos o dispositivos dentro de una red doméstica o privada. En general, en redes que no sean la propia Internet y utilicen su mismo protocolo (el mismo "idioma" de comunicación).


#####¿Qué diferencia hay entre NAT estática, NAT dinámica y PAT? ¿Cuál es la que se encuentra usualmente en los routers de los hogares?


NAT

Estática
Una dirección IP privada se traduce siempre en una misma dirección IP pública. Este modo de funcionamiento permitiría a un host dentro de la red ser visible desde Internet. (Ver imagen anterior)

Dinámica
El router tiene asignadas varias direcciones IP públicas, de modo que cada dirección IP privada se mapea usando una de las direcciones IP públicas que el router tiene asignadas, de modo que a cada dirección IP privada le corresponde al menos una dirección IP pública.


PAT
Es una extensión a la traducción de direcciones de red (NAT) que permite que varios dispositivos en una red de área local (LAN) que se asignan a un solo público la dirección IP
El objetivo del PAT es la conservación de direcciones IP.















































