# Práctica 4
*León Canto Angel
*311253171

***

##Ejemplo

Para ejecutar esta practica tenemos que posicionarnos en la carpeta que se encuentra el codigo fuente desde la terminal ejemplo: tareas-redes/content/tarea/AngelLeon


Craamos una nueva Red en VirtualBox en este caso lo hacemos desde 
Archivos-> referecias-> Red

![](Imagenes/P4-R-1.png)

Agregamos la Red


![](Imagenes/P4-R-0.png)

Red

![](Imagenes/P4-Nat.png)


###Configuración de interfaz de red
Prender máquina y verificar nombre de interfaz de red con el comando ip a

![](Imagenes/deb1.png)

![](Imagenes/resolv.png)

![](Imagenes/P4-Interface.png)


## Se crea  otra  maquina virtual con la configuración de red

![](Imagenes/P4-Re-ipa.png)

![](Imagenes/P4-Re-ipa2.png)

## PXE

Se modifican los archivos para PXE


![](Imagenes/Cliente1.png)

![](Imagenes/tft1.png)

![](Imagenes/wiretft.png)


##Preguntas

¿Cómo funciona el protocolo DHCP?

DHCP (Dynamic Host Configuration Protocol) es un conjunto de reglas para dar direccionesIP y opciones de configuración a ordenadores y estaciones de trabajo en una red. Una dirección IP es un número queidentifica de forma única a un ordenador en la red

Funcionamiento de DHCPDHCP funciona sobre un servidor central (servidor, estación de trabajo o incluso un PC) el cual asigna direcciones IP aotras máquinas de la red. Este protocolo puede entregar información IP en una LAN o entre varias VLAN. Esta tecnologíareduce el trabajo de un administrador, que de otra manera tendría que visitar todos los ordenadores o estaciones de trabajouno por uno. Para introducir la configuración IP consistente en IP, máscara, gateway, DNS, etc.Un servidor DHSC (DHCP Server) es un equipo en una red que está corriendo un servicio DHCP. Dicho servicio semantiene a la escucha de peticiones broadcast DHCP. Cuando una de estas peticiones es oída, el servidor responde conuna dirección IP y opcionalmente con información adicional.


2.-¿Cómo se filtra el tráfico del protocolo DHCP en una captura de tráfico en Wireshark y en tcpdump?

Para filtrar los paquetes en wireshark solo es necesrio  poner en los filtrso 
bootp   ya que  viene del protocolo de http 


3.-¿Qué es el estándar PXE y cuál es su utilidad?

Como se muestra en el título PXE (Preboot eXecution Environment) es un entorno de ejecución antes del arranque para la propia tarjeta de red. Hay tarjetas de red que contienen un chip con este sistema de arranque y le permiten a la tarjeta iniciarse (por decirlo de alguna manera) sin tener un SO en carga y consultar si existen servidores de DHCP con información que le ayude a arrancar el equipo para multiples funciones.

Una de las funciones por ejemplo es la que se ha comentado en el post de RIS (Remote Installation Server). Instalando un servidor con DHCP y RIS es posible realizar una instalación en red para un equipo sin necesidad de cargar el disco, incluso de iniciar una instalación desatendida (muy útil por ejemplo en redes enormes con decenas de puestos) es decir, iniciar una instalación preconfigurada antes en el servidor.



