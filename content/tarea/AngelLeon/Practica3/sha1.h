#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#include <errno.h>

#include <openssl/sha.h>





void sha_hash_string (char hash[SHA_DIGEST_LENGTH], char outputBuffer[65]){

    for(int i = 0; i < SHA_DIGEST_LENGTH; i++) {
        sprintf(outputBuffer + (i * 2), "%02x", (unsigned char)hash[i]);
    }

    outputBuffer[64] = 0;
}

void sha(char *string, char outputBuffer[65]){
    
    int len;
    unsigned char  hash[SHA_DIGEST_LENGTH];
    SHA_CTX sha;
    SHA1_Init(&sha);
    len=strlen(string);
    SHA1_Update(&sha, string,len);
    SHA1_Final(hash, &sha);
    


    
    //int i = 0;
    for(int i = 0; i < SHA_DIGEST_LENGTH; i++){
        sprintf(outputBuffer + (i * 2), "%02x", (unsigned char)hash[i]);
    }

    outputBuffer[64] = 0;
}


