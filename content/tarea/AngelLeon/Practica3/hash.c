#include <stdio.h>
#include "md5.h"
#include "sha1.h"
#include "sha256.h"

typedef enum _boolean {FALSE,TRUE} boolean; 



boolean esTipo(char *arg, char *id, int length){
    for (int i = 0; i < length; i++){
        if(arg[i] != id[i]){
            return FALSE; 
        }
    }
    return TRUE; 
}




int main(int argv, char *argc[]){
    
    if (argv > 2){
       
        char sha256sum[] = "sha256"; 
        char sha1sum[] = "sha1";
        char md5sum[] = "md5";
        char calc_hash[65];
        
        for (int i = 2; i < argv; i++){
            FILE *fich = fopen(argc[i], "r");
            
            if (esTipo(argc[1], md5sum, 3)){
                if (fich){
                    md5_file(argc[i],calc_hash);
                }
                else{
                    md5(argc[i],calc_hash);
                }
            }
            else if (esTipo(argc[1], sha1sum, 4)){
                if (fich){
                    sha_file(argc[i],calc_hash);
                }
                else{
                    sha(argc[i],calc_hash);
                }
            }
            else if (esTipo(argc[1], sha256sum, 6)){
                if (fich){
                    sha256_file(argc[i],calc_hash);
                }
                else{
                    sha256(argc[i],calc_hash);
                }
            }

            printf("%s ",calc_hash);
            if (!fich){
                printf("Hash de la cadena: ");
            }
            else{
                printf(" --> ");
            }
            printf("\"%s\"\n",argc[i]);
        }
    }
    return 0;
}



