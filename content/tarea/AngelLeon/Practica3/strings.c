#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <fcntl.h>

bool imprimible(unsigned char c){
    if(c >= 0x20 && c <= 0x7e || c == 0x09){
        return true;
    }
    return false;
}

int main(int argc, char * argv []){
    char buffer[1000];
    char *p = buffer;
    char ch;
    int fd;
    
    if(argc < 2) {
        printf("%sArchivo ", argv[0]);
        return 1;
    }


    fd = open(argv[1], O_RDONLY);
    if(0 <= fd){
        while(1 == read(fd, &ch, 1)) {
            if(imprimible(ch) && (p - buffer < sizeof(buffer) - 3)) {
                *p++ = ch;
            }

            else{
                if(p - buffer >= 4){
                    *p++ = '\n';
                    *p++ = '\0';
                    printf("%s", buffer);
                }
                p = buffer;
            }
        }
        if(p - buffer >= 4){
            *p++ = '\n';
            *p++ = '\0';
            printf("%s", buffer);
        }
        close(fd);
    } else {
        printf("No se puede abiri el Archivo %s\n", argv[1]);
        return 1;
    }
    
    return 0;
}
