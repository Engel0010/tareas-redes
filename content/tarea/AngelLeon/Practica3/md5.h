#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include <errno.h>
#include <openssl/md5.h>



#ifndef MD5_DIGEST_LENGTH
#define MD5_DIGEST_LENGTH (16)
#endif 




void md5(char *string, char outputBuffer[65]){
    
    unsigned char  hash[MD5_DIGEST_LENGTH];
    MD5_CTX md5;
    MD5_Init(&md5);
    int len = strlen(string);
    MD5_Update(&md5, string,len);
    MD5_Final(hash, &md5);
    
    
    for(int i = 0; i < MD5_DIGEST_LENGTH; i++)    {
        sprintf(outputBuffer + (i * 2), "%02x", (unsigned char)hash[i]);
    }

    outputBuffer[64] = 0;
}



void md5_hash_string (char hash[MD5_DIGEST_LENGTH], char outputBuffer[65]){
    
    for(int i = 0; i < MD5_DIGEST_LENGTH; i++)    {
        sprintf(outputBuffer + (i * 2), "%02x", (unsigned char)hash[i]);
    }
    
    outputBuffer[64] = 0;
}




int md5_file(char *path, char outputBuffer[65]){
    FILE *fich = fopen(path, "rb");

    if(!fich){
         return -534;
    }

    unsigned char hash[MD5_DIGEST_LENGTH];
    MD5_CTX md5;
    MD5_Init(&md5);
    
    const int bufSize = 32768;
    unsigned char *buffer = malloc(bufSize);
    
    int bytesRead = 0;

    if(!buffer){
        return ENOMEM;
    }

    while((bytesRead = fread(buffer, 1, bufSize, fich))){
        MD5_Update(&md5, buffer, bytesRead);
    }

    MD5_Final(hash, &md5);
    md5_hash_string(hash, outputBuffer);
    fclose(fich);
    free(buffer);
    
    return 0;
}
