
struct nodo{
    void * elemento;
    struct nodo * siguiente;
};

struct  lista
{
    struct nodo * cabeza;
    int longitud;
};




//Agrega un elemento al final de la lista.
void * agrega_elemento(struct lista * lista, void * elemento){
    
    struct nodo * nuevo;
    struct nodo * aux;
    nuevo = malloc(sizeof(nuevo));
    nuevo -> elemento = elemento;
        aux =  lista -> cabeza;
    if(lista->longitud != 0 && lista ->cabeza != 0){        
        while(aux->siguiente != 0){
            aux = aux -> siguiente;
        
            }
        aux ->siguiente = nuevo;
        lista -> longitud++;
    }
    else{
        lista -> cabeza = nuevo;
        lista -> longitud++;
    }

}




//Permite obtener el n-elemento de la lista, empezando desde 0.
void * obten_elemento(struct lista * lista, int n){
   int i = 0;
   int * a = &i;
    struct nodo * aux;
    aux = lista ->cabeza;
    if(n == 0 && lista->longitud != 0){
        return aux -> elemento;
    }
    if(aux -> siguiente != 0){

        for(i; i<=n; i++){
            aux = aux -> siguiente;
        }
      
        return aux -> elemento;
    }else{
        return a;
    } 
}




//Elimina el n-elemento de la lista, empezando desde 0.
void * elimina_elemento(struct lista * lista, int n){
    
    int i = 0;
    struct nodo * aux;
    struct nodo * nuevo;
    aux = lista -> cabeza;
    if(lista->longitud != 0 && lista->longitud >= n){
        if (n == 0) {
            nuevo = aux->elemento;
            lista -> cabeza = aux -> siguiente;
            lista -> longitud--;
        } else {
            if (n == (lista -> longitud -1)){
                while(aux->siguiente->siguiente != 0){
                    aux = aux -> siguiente;
                    i++;
                }
                nuevo = aux -> siguiente->elemento;
                lista -> longitud--;
            } else {
                for(int e=0;  e<n; e++){
                    aux = aux -> siguiente;
                    }
              
                nuevo = aux -> siguiente->elemento;
                aux -> siguiente = aux -> siguiente -> siguiente;
                lista -> longitud--;
            }
        }
    }

    return nuevo;
}




//Aplica una funcion a cada elemento de la lista
void aplica_funcion(struct lista * lista, void (*f)(void *)){
   
    struct nodo * aux;
    aux = lista->cabeza;
    while(aux->siguiente != 0){
        
        f(aux->elemento);
        aux = aux ->siguiente;
    }
    f(aux->elemento);        
}