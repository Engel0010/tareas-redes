#include <stdio.h>
#include <stdlib.h>

int main( int argc, char *argv[] ) {
  int numeros[argc];
  
  for (int i = 1; i < argc; i++ ){
    int num = atoi(argv[i]);
    numeros[i] = num;
  }


  for (int i = 1; i <= argc; i++ ){
    for (int j = 1; j <= argc; j++ ){
      if (numeros[j] > numeros[j+1]){
        int aux = numeros[j];
        numeros[j] = numeros[j+1];
        numeros[j+1] = aux;
      }
    }
  }
  
  for (int i = 2; i <= argc; i++ ){
    printf("%i\n",numeros[i] );
  }

  return 0;
}